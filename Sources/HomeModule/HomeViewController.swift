//
//  HomeViewController.swift
//  
//
//  Created by Shivkumar on 16/01/23.
//

import UIKit

public class HomeViewController: UIViewController {

    
    @IBOutlet weak var welcomeLabel: UILabel!
    public var userId: String?
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        welcomeLabel.text = "Welcome to Home Module for User ID -- \(userId ?? "")"

        // Do any additional setup after loading the view.
    }
    static public func createHomeViewController() -> HomeViewController {
        
        let vc = HomeViewController(nibName: "HomeViewController", bundle: Bundle.module)
        return vc
    }
    public func getHomeScreenName() -> String {
        return "Home"
    }
    
    public func getStringToTestPipeline() -> String {
        return "Pipeline"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
